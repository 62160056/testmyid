<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9a244507-bfbc-4043-b55f-d16fa9f30437</testSuiteGuid>
   <testCaseLink>
      <guid>c3856244-9119-4b60-a955-c434e6cc8557</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6b63285-4213-45ac-81e9-c16ce73baa04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0298401a-5a78-4401-9569-53d2b03da093</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7f83b7d-c7be-4749-a35c-b74279564277</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47ccc778-fca1-494f-bdf9-085bdd5a8090</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 2 (Not Fail)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf3250c2-2b5a-4959-a498-d055cc49d4c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 2.5 To Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7526e6df-4183-4d2c-95f9-6c47e0f01135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f782d934-dc2a-4b84-89df-0cf89fa3eb64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43892c42-6c7f-4f86-9c6d-ea33874297d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Fail 5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e0c5519-51fa-4047-9e5a-f72d09e7d15f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Re Password Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
